package package1;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

public class JavaAST {
	
	public static String getFileContent(String filePath)throws FileNotFoundException, IOException{
		BufferedReader br = new BufferedReader(new FileReader(filePath));
		StringBuilder sb = new StringBuilder();
		String line = br.readLine();
		while (line != null) {
			sb.append(line);
			sb.append(System.lineSeparator());
			line =br.readLine();
		}
		return sb.toString();
	}
	
	public static void main(String[] args) {
		String filePath ="E:\\logiciel\\java_montpellier_JEE_2021\\workspace\\projectPoint\\src\\package1\\Point.java";
	
		ASTParser parser = ASTParser.newParser(AST.JLS8);
		try {
			char[] fileContent = getFileContent(filePath).toCharArray();
			parser.setSource(fileContent);
			CompilationUnit cu = (CompilationUnit) parser.createAST(null);
			cu.accept(new ASTVisitor() {
			
		public boolean visit(MethodDeclaration methodNode) {
				String method =methodNode.getName().toString();
				System.out.println("Method :"+method);
				return false;
			}
		public boolean visit(VariableDeclarationFragment variableNode) {
		
			// liste des attributs
				String name = variableNode.getName().toString();
				System.out.println("Attribute Name : "+name);
				
			// encapsulation des attributs
				String encapsulation = variableNode.getParent().toString();
				System.out.println("encapsulation  : "+ encapsulation);
				return false;
			}
			

		public boolean visit(TypeDeclaration typeNode) {
			String className =typeNode.getName().toString();
			System.out.println("className :"+className);
			String superClassName = typeNode.getSuperclassType().toString();
			System.out.println("Super Class Name : "+superClassName);
			return true;
		}
	
		
			});
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
	
	
	
	
	}

}
